<?php

namespace RtfHtmlPhp\Html;

class Font
{
    public ?string $family   = null;
    public ?string $name     = null;
    public ?string $charset  = null;
    public ?string $codepage = null;

    public function toStyle(): string
    {
        $list = [];
        if ($this->name) {
            $list[] = $this->name;
        }

        if ($this->family) {
            $list[] = $this->family;
        }

        if (sizeof($list) == 0) {
            return "";
        }

        return "font-family:" . implode(' ', $list) . ";";
    }
}
